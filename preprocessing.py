import re
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from nltk.stem.porter import PorterStemmer
import string
import fnmatch
import os
import pickle as pkl

from email.parser import Parser

dict = ["aa", "ab", "abil", "abl", "about", "abov", "absolut", "abus", "ac", "accept", "access", "accord", "account",
        "achiev", "acquir", "across", "act", "action", "activ", "actual", "ad", "adam", "add", "addit", "address",
        "administr", "adult", "advanc", "advantag", "advertis", "advic", "advis", "ae", "af", "affect", "affili",
        "afford", "africa", "after", "ag", "again", "against", "agenc", "agent", "ago", "agre", "agreement", "aid",
        "air", "al", "alb", "align", "all", "allow", "almost", "alon", "along", "alreadi", "alsa", "also", "altern",
        "although", "alwai", "am", "amaz", "america", "american", "among", "amount", "amp", "an", "analysi", "analyst",
        "and", "ani", "anim", "announc", "annual", "annuiti", "anoth", "answer", "anti", "anumb", "anybodi", "anymor",
        "anyon", "anyth", "anywai", "anywher", "aol", "ap", "apolog", "app", "appar", "appear", "appl", "appli",
        "applic", "appreci", "approach", "approv", "apt", "ar", "archiv", "area", "aren", "argument", "arial", "arm",
        "around", "arrai", "arriv", "art", "articl", "artist", "as", "ascii", "ask", "asset", "assist", "associ",
        "assum", "assur", "at", "atol", "attach", "attack", "attempt", "attent", "attornei", "attract", "audio", "aug",
        "august", "author", "auto", "autom", "automat", "avail", "averag", "avoid", "awai", "awar", "award", "ba",
        "babi", "back", "background", "backup", "bad", "balanc", "ban", "bank", "bar", "base", "basenumb", "basi",
        "basic", "bb", "bc", "bd", "be", "beat", "beberg", "becaus", "becom", "been", "befor", "begin", "behalf",
        "behavior", "behind", "believ", "below", "benefit", "best", "beta", "better", "between", "bf", "big", "bill",
        "billion", "bin", "binari", "bit", "black", "blank", "block", "blog", "blood", "blue", "bnumber", "board",
        "bodi", "boi", "bonu", "book", "boot", "border", "boss", "boston", "botan", "both", "bottl", "bottom",
        "boundari", "box", "brain", "brand", "break", "brian", "bring", "broadcast", "broker", "browser", "bug", "bui",
        "build", "built", "bulk", "burn", "bush", "busi", "but", "button", "by", "byte", "ca", "cabl", "cach", "calcul",
        "california", "call", "came", "camera", "campaign", "can", "canada", "cannot", "canon", "capabl", "capillari",
        "capit", "car", "card", "care", "career", "carri", "cartridg", "case", "cash", "cat", "catch", "categori",
        "caus", "cb", "cc", "cd", "ce", "cell", "cent", "center", "central", "centuri", "ceo", "certain", "certainli",
        "cf", "challeng", "chanc", "chang", "channel", "char", "charact", "charg", "charset", "chat", "cheap", "check",
        "cheer", "chief", "children", "china", "chip", "choic", "choos", "chri", "citi", "citizen", "civil", "claim",
        "class", "classifi", "clean", "clear", "clearli", "click", "client", "close", "clue", "cnet", "cnumber", "co",
        "code", "collect", "colleg", "color", "com", "combin", "come", "comfort", "command", "comment", "commentari",
        "commerci", "commiss", "commit", "common", "commun", "compani", "compar", "comparison", "compat", "compet",
        "competit", "compil", "complet", "comprehens", "comput", "concentr", "concept", "concern", "condit", "conf",
        "confer", "confid", "confidenti", "config", "configur", "confirm", "conflict", "confus", "congress", "connect",
        "consid", "consolid", "constitut", "construct", "consult", "consum", "contact", "contain", "content", "continu",
        "contract", "contribut", "control", "conveni", "convers", "convert", "cool", "cooper", "copi", "copyright",
        "core", "corpor", "correct", "correspond", "cost", "could", "couldn", "count", "countri", "coupl", "cours",
        "court", "cover", "coverag", "crash", "creat", "creativ", "credit", "critic", "cross", "cultur", "current",
        "custom", "cut", "cv", "da", "dagga", "dai", "daili", "dan", "danger", "dark", "data", "databas", "datapow",
        "date", "dave", "david", "dc", "de", "dead", "deal", "dear", "death", "debt", "decad", "decid", "decis",
        "declar", "declin", "decor", "default", "defend", "defens", "defin", "definit", "degre", "delai", "delet",
        "deliv", "deliveri", "dell", "demand", "democrat", "depart", "depend", "deposit", "describ", "descript",
        "deserv", "design", "desir", "desktop", "despit", "detail", "detect", "determin", "dev", "devel", "develop",
        "devic", "di", "dial", "did", "didn", "diet", "differ", "difficult", "digit", "direct", "directli", "director",
        "directori", "disabl", "discount", "discov", "discoveri", "discuss", "disk", "displai", "disposit", "distanc",
        "distribut", "dn", "dnumber", "do", "doc", "document", "doe", "doer", "doesn", "dollar", "dollarac",
        "dollarnumb", "domain", "don", "done", "dont", "doubl", "doubt", "down", "download", "dr", "draw", "dream",
        "drive", "driver", "drop", "drug", "due", "dure", "dvd", "dw", "dynam", "ea", "each", "earli", "earlier",
        "earn", "earth", "easi", "easier", "easili", "eat", "eb", "ebai", "ec", "echo", "econom", "economi", "ed",
        "edg", "edit", "editor", "educ", "eff", "effect", "effici", "effort", "either", "el", "electron", "elimin",
        "els", "email", "emailaddr", "emerg", "empir", "employ", "employe", "en", "enabl", "encod", "encourag", "end",
        "enemi", "enenkio", "energi", "engin", "english", "enhanc", "enjoi", "enough", "ensur", "enter", "enterpris",
        "entertain", "entir", "entri", "enumb", "environ", "equal", "equip", "equival", "error", "especi", "essenti",
        "establish", "estat", "estim", "et", "etc", "euro", "europ", "european", "even", "event", "eventu", "ever",
        "everi", "everyon", "everyth", "evid", "evil", "exactli", "exampl", "excel", "except", "exchang", "excit",
        "exclus", "execut", "exercis", "exist", "exmh", "expand", "expect", "expens", "experi", "expert", "expir",
        "explain", "explor", "express", "extend", "extens", "extra", "extract", "extrem", "ey", "fa", "face", "fact",
        "factor", "fail", "fair", "fall", "fals", "famili", "faq", "far", "fast", "faster", "fastest", "fat", "father",
        "favorit", "fax", "fb", "fd", "featur", "feder", "fee", "feed", "feedback", "feel", "femal", "few", "ffffff",
        "ffnumber", "field", "fight", "figur", "file", "fill", "film", "filter", "final", "financ", "financi", "find",
        "fine", "finish", "fire", "firewal", "firm", "first", "fit", "five", "fix", "flag", "flash", "flow", "fnumber",
        "focu", "folder", "folk", "follow", "font", "food", "for", "forc", "foreign", "forev", "forget", "fork", "form",
        "format", "former", "fortun", "forward", "found", "foundat", "four", "franc", "free", "freedom", "french",
        "freshrpm", "fri", "fridai", "friend", "from", "front", "ftoc", "ftp", "full", "fulli", "fun", "function",
        "fund", "further", "futur", "ga", "gain", "game", "gari", "garrigu", "gave", "gcc", "geek", "gener", "get",
        "gif", "gift", "girl", "give", "given", "global", "gnome", "gnu", "gnupg", "go", "goal", "god", "goe", "gold",
        "gone", "good", "googl", "got", "govern", "gpl", "grand", "grant", "graphic", "great", "greater", "ground",
        "group", "grow", "growth", "gt", "guarante", "guess", "gui", "guid", "ha", "hack", "had", "half", "ham", "hand",
        "handl", "happen", "happi", "hard", "hardwar", "hat", "hate", "have", "haven", "he", "head", "header",
        "headlin", "health", "hear", "heard", "heart", "heaven", "hei", "height", "held", "hello", "help", "helvetica",
        "her", "herba", "here", "hermio", "hettinga", "hi", "high", "higher", "highli", "highlight", "him", "histori",
        "hit", "hold", "home", "honor", "hope", "host", "hot", "hour", "hous", "how", "howev", "hp", "html", "http",
        "httpaddr", "huge", "human", "hundr", "ibm", "id", "idea", "ident", "identifi", "idnumb", "ie", "if", "ignor",
        "ii", "iii", "iiiiiiihnumberjnumberhnumberjnumberhnumb", "illeg", "im", "imag", "imagin", "immedi", "impact",
        "implement", "import", "impress", "improv", "in", "inc", "includ", "incom", "increas", "incred", "inde",
        "independ", "index", "india", "indian", "indic", "individu", "industri", "info", "inform", "initi", "inlin",
        "innov", "input", "insert", "insid", "instal", "instanc", "instant", "instead", "institut", "instruct", "insur",
        "int", "integr", "intel", "intellig", "intend", "interact", "interest", "interfac", "intern", "internet",
        "interview", "into", "intro", "introduc", "inumb", "invest", "investig", "investor", "invok", "involv", "ip",
        "ireland", "irish", "is", "island", "isn", "iso", "isp", "issu", "it", "item", "itself", "jabber", "jame",
        "java", "jim", "jnumberiiiiiiihepihepihf", "job", "joe", "john", "join", "journal", "judg", "judgment", "jul",
        "juli", "jump", "june", "just", "justin", "keep", "kei", "kept", "kernel", "kevin", "keyboard", "kid", "kill",
        "kind", "king", "kingdom", "knew", "know", "knowledg", "known", "la", "lack", "land", "languag", "laptop",
        "larg", "larger", "largest", "laser", "last", "late", "later", "latest", "launch", "law", "lawrenc", "le",
        "lead", "leader", "learn", "least", "leav", "left", "legal", "lender", "length", "less", "lesson", "let",
        "letter", "level", "lib", "librari", "licens", "life", "lifetim", "light", "like", "limit", "line", "link",
        "linux", "list", "listen", "littl", "live", "ll", "lo", "load", "loan", "local", "locat", "lock", "lockergnom",
        "log", "long", "longer", "look", "lose", "loss", "lost", "lot", "love", "low", "lower", "lowest", "lt", "ma",
        "mac", "machin", "made", "magazin", "mai", "mail", "mailer", "main", "maintain", "major", "make", "maker",
        "male", "man", "manag", "mani", "manual", "manufactur", "map", "march", "margin", "mark", "market", "marshal",
        "mass", "master", "match", "materi", "matter", "matthia", "mayb", "me", "mean", "measur", "mechan", "media",
        "medic", "meet", "member", "membership", "memori", "men", "mention", "menu", "merchant", "messag", "method",
        "mh", "michael", "microsoft", "middl", "might", "mike", "mile", "militari", "million", "mime", "mind", "mine",
        "mini", "minimum", "minut", "miss", "mistak", "mobil", "mode", "model", "modem", "modifi", "modul", "moment",
        "mon", "mondai", "monei", "monitor", "month", "monthli", "more", "morn", "mortgag", "most", "mostli", "mother",
        "motiv", "move", "movi", "mpnumber", "mr", "ms", "msg", "much", "multi", "multipart", "multipl", "murphi",
        "music", "must", "my", "myself", "name", "nation", "natur", "nbsp", "near", "nearli", "necessari", "need",
        "neg", "net", "netscap", "network", "never", "new", "newslett", "next", "nextpart", "nice", "nigeria", "night",
        "no", "nobodi", "non", "none", "nor", "normal", "north", "not", "note", "noth", "notic", "now", "nt", "null",
        "number", "numbera", "numberam", "numberanumb", "numberb", "numberbit", "numberc", "numbercb", "numbercbr",
        "numbercfont", "numbercli", "numbercnumb", "numbercp", "numberctd", "numberd", "numberdari", "numberdnumb",
        "numberenumb", "numberf", "numberfb", "numberff", "numberffont", "numberfp", "numberftd", "numberk", "numberm",
        "numbermb", "numberp", "numberpd", "numberpm", "numberpx", "numberst", "numberth", "numbertnumb", "numberx",
        "object", "oblig", "obtain", "obvious", "occur", "oct", "octob", "of", "off", "offer", "offic", "offici",
        "often", "oh", "ok", "old", "on", "onc", "onli", "onlin", "open", "oper", "opinion", "opportun", "opt", "optim",
        "option", "or", "order", "org", "organ", "origin", "os", "osdn", "other", "otherwis", "our", "out", "outlook",
        "output", "outsid", "over", "own", "owner", "oz", "pacif", "pack", "packag", "page", "pai", "paid", "pain",
        "palm", "panel", "paper", "paragraph", "parent", "part", "parti", "particip", "particular", "particularli",
        "partit", "partner", "pass", "password", "past", "patch", "patent", "path", "pattern", "paul", "payment", "pc",
        "peac", "peopl", "per", "percent", "percentag", "perfect", "perfectli", "perform", "perhap", "period", "perl",
        "perman", "permiss", "person", "pgp", "phone", "photo", "php", "phrase", "physic", "pick", "pictur", "piec",
        "piiiiiiii", "pipe", "pjnumber", "place", "plai", "plain", "plan", "planet", "plant", "planta", "platform",
        "player", "pleas", "plu", "plug", "pm", "pocket", "point", "polic", "polici", "polit", "poor", "pop", "popul",
        "popular", "port", "posit", "possibl", "post", "potenti", "pound", "powel", "power", "powershot", "practic",
        "pre", "predict", "prefer", "premium", "prepar", "present", "presid", "press", "pretti", "prevent", "previou",
        "previous", "price", "principl", "print", "printabl", "printer", "privaci", "privat", "prize", "pro", "probabl",
        "problem", "procedur", "process", "processor", "procmail", "produc", "product", "profession", "profil",
        "profit", "program", "programm", "progress", "project", "promis", "promot", "prompt", "properti", "propos",
        "proprietari", "prospect", "protect", "protocol", "prove", "proven", "provid", "proxi", "pub", "public",
        "publish", "pudg", "pull", "purchas", "purpos", "put", "python", "qnumber", "qualifi", "qualiti", "quarter",
        "question", "quick", "quickli", "quit", "quot", "radio", "ragga", "rais", "random", "rang", "rate", "rather",
        "ratio", "razor", "razornumb", "re", "reach", "read", "reader", "readi", "real", "realiz", "realli", "reason",
        "receiv", "recent", "recipi", "recommend", "record", "red", "redhat", "reduc", "refer", "refin", "reg",
        "regard", "region", "regist", "regul", "regular", "rel", "relat", "relationship", "releas", "relev", "reliabl",
        "remain", "rememb", "remot", "remov", "replac", "repli", "report", "repositori", "repres", "republ", "request",
        "requir", "research", "reserv", "resid", "resourc", "respect", "respond", "respons", "rest", "result", "retail",
        "return", "reveal", "revenu", "revers", "review", "revok", "rh", "rich", "right", "risk", "road", "robert",
        "rock", "role", "roll", "rom", "roman", "room", "root", "round", "rpm", "rss", "rule", "run", "sa", "safe",
        "sai", "said", "sale", "same", "sampl", "san", "saou", "sat", "satellit", "save", "saw", "scan", "schedul",
        "school", "scienc", "score", "screen", "script", "se", "search", "season", "second", "secret", "section",
        "secur", "see", "seed", "seek", "seem", "seen", "select", "self", "sell", "seminar", "send", "sender",
        "sendmail", "senior", "sens", "sensit", "sent", "sep", "separ", "septemb", "sequenc", "seri", "serif", "seriou",
        "serv", "server", "servic", "set", "setup", "seven", "seventh", "sever", "sex", "sexual", "sf", "shape",
        "share", "she", "shell", "ship", "shop", "short", "shot", "should", "show", "side", "sign", "signatur",
        "signific", "similar", "simpl", "simpli", "sinc", "sincer", "singl", "sit", "site", "situat", "six", "size",
        "skeptic", "skill", "skin", "skip", "sleep", "slow", "small", "smart", "smoke", "smtp", "snumber", "so",
        "social", "societi", "softwar", "sold", "solut", "solv", "some", "someon", "someth", "sometim", "son", "song",
        "soni", "soon", "sorri", "sort", "sound", "sourc", "south", "space", "spain", "spam", "spamassassin", "spamd",
        "spammer", "speak", "spec", "special", "specif", "specifi", "speech", "speed", "spend", "sponsor", "sport",
        "spot", "src", "ssh", "st", "stabl", "staff", "stai", "stand", "standard", "star", "start", "state",
        "statement", "statu", "step", "steve", "still", "stock", "stop", "storag", "store", "stori", "strategi",
        "stream", "street", "string", "strip", "strong", "structur", "studi", "stuff", "stupid", "style", "subject",
        "submit", "subscrib", "subscript", "substanti", "success", "such", "suffer", "suggest", "suit", "sum",
        "summari", "summer", "sun", "super", "suppli", "support", "suppos", "sure", "surpris", "suse", "suspect",
        "sweet", "switch", "system", "tab", "tabl", "tablet", "tag", "take", "taken", "talk", "tape", "target", "task",
        "tax", "teach", "team", "tech", "technic", "techniqu", "technolog", "tel", "telecom", "telephon", "tell",
        "temperatur", "templ", "ten", "term", "termin", "terror", "terrorist", "test", "texa", "text", "than", "thank",
        "that", "the", "thei", "their", "them", "themselv", "then", "theori", "there", "therefor", "these", "thi",
        "thing", "think", "thinkgeek", "third", "those", "though", "thought", "thousand", "thread", "threat", "three",
        "through", "thu", "thursdai", "ti", "ticket", "tim", "time", "tip", "tire", "titl", "tm", "to", "todai",
        "togeth", "token", "told", "toll", "tom", "toner", "toni", "too", "took", "tool", "top", "topic", "total",
        "touch", "toward", "track", "trade", "tradit", "traffic", "train", "transact", "transfer", "travel", "treat",
        "tree", "tri", "trial", "trick", "trip", "troubl", "TRUE", "truli", "trust", "truth", "try", "tue", "tuesdai",
        "turn", "tv", "two", "type", "uk", "ultim", "un", "under", "understand", "unfortun", "uniqu", "unison", "unit",
        "univers", "unix", "unless", "unlik", "unlimit", "unseen", "unsolicit", "unsubscrib", "until", "up", "updat",
        "upgrad", "upon", "urgent", "url", "us", "usa", "usag", "usb", "usd", "usdollarnumb", "useless", "user", "usr",
        "usual", "util", "vacat", "valid", "valu", "valuabl", "var", "variabl", "varieti", "variou", "ve", "vendor",
        "ventur", "veri", "verifi", "version", "via", "video", "view", "virtual", "visa", "visit", "visual", "vnumber",
        "voic", "vote", "vs", "vulner", "wa", "wai", "wait", "wake", "walk", "wall", "want", "war", "warm", "warn",
        "warranti", "washington", "wasn", "wast", "watch", "water", "we", "wealth", "weapon", "web", "weblog", "websit",
        "wed", "wednesdai", "week", "weekli", "weight", "welcom", "well", "went", "were", "west", "what", "whatev",
        "when", "where", "whether", "which", "while", "white", "whitelist", "who", "whole", "whose", "why", "wi",
        "wide", "width", "wife", "will", "william", "win", "window", "wing", "winner", "wireless", "wish", "with",
        "within", "without", "wnumberp", "woman", "women", "won", "wonder", "word", "work", "worker", "world",
        "worldwid", "worri", "worst", "worth", "would", "wouldn", "write", "written", "wrong", "wrote", "www", "ximian",
        "xml", "xp", "yahoo", "ye", "yeah", "year", "yesterdai", "yet", "york", "you", "young", "your", "yourself",
        "zdnet", "zero", "zip", "ring@enron.com", "williams@enron.com", "semperger@enron.com", "solberg@enron.com",
        "meyers@enron.com", "gilbert-smith@enron.com", "dean@enron.com", "guzman@enron.com", "slinger@enron.com",
        "symes@enron.com", "salisbury@enron.com", "linder@enron.com", "scholtes@enron.com", "dasovich@enron.com",
        "crandall@enron.com", "gang@enron.com", "causholli@enron.com", "forney@enron.com", "taylor@enron.com",
        "stokley@enron.com", "panus@enron.com", "badeer@enron.com", "white@enron.com", "steffes@enron.com",
        "kitchen@enron.com", "grigsby@enron.com", "motley@enron.com", "swerzbin@enron.com", "richey@enron.com",
        "skilling@enron.com", "fleming@enron.com", "beck@enron.com", "horton@enron.com", "hayslett@enron.com",
        "lay@enron.com", "shapiro@enron.com", "shankman@enron.com", "derrick@enron.com", "giron@enron.com",
        "love@enron.com", "mclaughlin@enron.com", "keiser@enron.com", "allen@enron.com", "holst@enron.com",
        "lenhart@enron.com", "rodrigue@enron.com", "germany@enron.com", "buy@enron.com", "hyatt@enron.com",
        "kean@enron.com", "schwieger@enron.com", "rogers@enron.com", "mccarty@enron.com", "delainey@enron.com",
        "donohoe@enron.com", "pereira@enron.com", "shively@enron.com", "martin@enron.com", "presto@enron.com",
        "neal@enron.com", "sturm@enron.com", "cuilla@enron.com", "zipper@enron.com", "storey@enron.com",
        "cash@enron.com", "reitmeyer@enron.com", "bass@enron.com", "farmer@enron.com", "weldon@enron.com",
        "sanchez@enron.com", "hyvl@enron.com", "scott@enron.com", "dorland@enron.com", "baughman@enron.com",
        "hernandez@enron.com", "king@enron.com", "stepenovitch@enron.com", "whalley@enron.com", "kaminski@enron.com",
        "may@enron.com", "davis@enron.com", "tycholiz@enron.com", "quenet@enron.com",
        "benson@enron.com", "brawner@enron.com", "arnold@enron.com", "maggi@enron.com",
        "mims@enron.com", "pimenov@enron.com", "saibi@enron.com", "carson@enron.com", "zufferli@enron.com",
        "nemec@enron.com", "griffith@enron.com", "hain@enron.com", "smith@enron.com", "perlingiere@enron.com",
        "kuykendall@enron.com", "fischer@enron.com", "platter@enron.com", "bailey@enron.com", "parks@enron.com",
        "mann@enron.com", "hodge@enron.com", "townsend@enron.com", "hendrickson@enron.com", "sager@enron.com",
        "quigley@enron.com", "akin@enron.com", "heard@enron.com", "jones@enron.com", "shackleton@enron.com",
        "mckay@enron.com", "haedicke@enron.com", "arora@enron.com", "watson@enron.com", "corman@enron.com",
        "sanders@enron.com", "lewis@enron.com", "merris@enron.com", "ruscitti@enron.com",
        "ward@enron.com", "thomas@enron.com", "campbell@enron.com", "clair@enron.com", "dickson@enron.com",
        "whitt@enron.com", "fossum@enron.com", "donoho@enron.com", "harris@enron.com", "blair@enron.com",
        "staab@enron.com", "lucci@enron.com", "y'barbo@enron.com", "mcconnell@enron.com", "lokay@enron.com",
        "rapp@enron.com", "schoolcraft@enron.com", "geaccone@enron.com", "lokey@enron.com", "south@enron.com",
        "keavey@enron.com", "gay@enron.com", "wolfe@enron.com", "ermis@enron.com",
        "lavorato@enron.com", "tholt@enron.com"]


def remove_whitespaces(s):
    return "".join(s.split())


stemmer = PorterStemmer()


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    # tokens = [i for i in tokens if i not in string.punctuation]
    stems = stem_tokens(tokens, stemmer)
    return stems


def extract_email_names(text):
    emails = re.findall(r'[\w\.-]+@[\w\.-]+', text)

    # print emails
    res = []
    for line in emails:
        try:
            # match = re.search(r'[\w\.-]+@[\w\.-]+', email)
            # line = match.group(0)
            data = line.split('@')
            name = data[0]
            domain = data[1]
            names = name.split('.')

            res.append(names[-1] + '@' + domain)
        except:
            print "Skipped email addr"

    return " ".join(res)



def extract_content_from_email(file_name):
    f = open(file_name, 'r')

    lines = []
    for x in f.readlines():
        lines.append(x)

    f.close()

    res = {'to': '', 'subject': '', 'body': '', 'from': ''}
    hasTo = False
    isTo = False
    isInSubject = False
    hasSubject = False
    isBody = False
    hasFrom = False

    body_lines = []

    # extract to
    for line in lines:
        if (('From:' in line) and (not 'X-From:' in line) and (not hasFrom)):
            res['from'] = line[6:]
            hasFrom = True

        if (isTo):
            if ('Subject' in line):
                isTo = False
            else:
                res['to'] += ' ' + line.strip()

        if (("To:" in line) and (not hasTo)):
            res['to'] = line[4:]
            hasTo = True
            isTo = True

        if (isInSubject):
            if (('Mime-Version' in line) or ('Cc:' in line)):
                isInSubject = False
            else:
                res['subject'] += ' ' + line

        if (('Subject:' in line) and (not hasSubject)):
            res['subject'] = line[9:]
            hasSubject = True
            isInSubject = True

        if (isBody):
            body_lines.append(line)

        if ('X-FileName:' in line):
            isBody = True

    res['body'] = " ".join(body_lines)

    res['to'] = remove_whitespaces(res['to'])
    res['to'] = extract_email_names(res['to'])

    res['from'] = remove_whitespaces(res['from'])
    res['from'] = extract_email_names(res['from'])

    res['subject'] = remove_whitespaces(res['subject'])
    res['body'] = remove_whitespaces(res['body'])

    # regex
    res['body'] = re.sub(r'<[^<>]+>', ' ', res['body'])
    res['body'] = re.sub(r'[^a-zA-Z0-9]', ' ', res['body'])

    res['body'] = res['body'].lower()
    res['body'] = " ".join(res['body'].split())

    res['feature'] = " ".join([res['body'], res['subject'], res['to']])

    return res

def extract_content_from_email_2(filename):
    f = open(filename, 'r')
    data = f.read()
    f.close()

    email_data = Parser().parsestr(data)
    res = {'to': '', 'subject': '', 'body': '', 'from': ''}

    res['to'] = email_data['to']
    # print res['to']
    if ('@' in res['to']):
        res['to'] = extract_email_names(res['to'])

    res['from'] = extract_email_names(email_data['from'])
    res['subject'] = remove_whitespaces(res['subject'])
    res['body'] = email_data.get_payload()

    res['body'] = re.sub(r'<[^<>]+>', '', res['body'])
    res['body'] = re.sub(r'[^a-zA-Z]', '', res['body'])

    res['body'] = res['body'].lower()
    # res['body'] = " ".join(res['body'].split())

    res['feature'] = " ".join([res['body'], res['subject'], res['to']])

    return res

if __name__ == "__main__":
    # prefix
    prefix = '/home/hendi/MLAnak2/'

    matches = []
    for root, dirnames, filenames in os.walk(prefix):
        for filename in fnmatch.filter(filenames, '*.'):
            matches.append(os.path.join(root, filename))

    feature_file = open(prefix + 'train_X.pkl', 'w+')
    label_file = open(prefix + 'train_Y.pkl', 'w+')

    # set up count vectorizer
    vec = CountVectorizer(tokenizer=tokenize, stop_words='english', vocabulary=dict)
    words = dict

    skipped = 0

    X = []
    y = []

    for i in range(len(matches)):
        try:
            filename = matches[i]
            # print "test"
            percent = (float(i) * 100)/float(len(matches))
            print "Processing file {}/{} - {}%".format(i, len(matches), percent)

            res = extract_content_from_email(filename)
            # body_trans = vec.transform([res['feature']])
            # body_x = body_trans.toarray()[0]

            X.append(res['feature'])
            y.append(res['from'])

            # feature_file.write(",".join([str(x) for x in body_x]) + '\n')
            # label_file.write(res['from'] + '\n')
        except:
            print "Skipped"
            skipped += 1

        # break

    print "Total skipped {}".format(skipped)

    pkl.dump(X, feature_file)
    pkl.dump(y, label_file)

    feature_file.close()
    label_file.close()