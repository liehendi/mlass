import pickle
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, accuracy_score

data_to_use = 'X_tfidf.pkl'
model_to_save = 'naive_bayes.pkl'
param_naive = {'alpha':[0.1, 1.0, 10.0]}
param_logistic_reg = {''}

X = pickle.load(open(data_to_use, 'r'))
y = pickle.load(open('train_Y.pkl', 'r'))
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42, test_size=0.25)

print "Training started"

# naive bayes CLF
clf = MultinomialNB()
# cv = GridSearchCV()

clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)
print classification_report(y_test, y_pred)
print accuracy_score(y_test, y_pred)

pickle.dump(clf, open(model_to_save, 'w+'))

print "Model saved!"
